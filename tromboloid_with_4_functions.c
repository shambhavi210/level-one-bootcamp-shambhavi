//WAP to find the volume of a tromboloid using 4 functions.
#include<stdio.h>
 
float input(char s[20]){
    float n;
    printf("Enter value of %s : ",s);
    scanf("%f",&n);
    return n;
}

float compute_volume(float h, float d, float b){
    float vol;
    vol = ((1.0/3)*((h*d)+d))/b;
    return vol;
}

void display(float h, float d, float b, float vol){
	printf("Volume of the tromboloid is: %f ",vol);
}

int main(){
    float d1,d2,d3,vol;
    d1 = input("height");
    d2 = input("depth");
    d3 = input("breadth");
    vol = compute_volume(d1,d2,d3);
    display(d1,d2,d3,vol);
    return 0;
}