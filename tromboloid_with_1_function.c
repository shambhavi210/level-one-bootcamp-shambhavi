//Write a program to find the volume of a tromboloid using one function
#include<stdio.h>
int main(){
	float h,d,b,vol;
	printf("Enter height of the tromboloid: ");
	scanf("%f", &h);
	printf("Enter depth of the tromboloid: ");
	scanf("%f", &d);
	printf("Enter breadth of the tromboloid: ");
	scanf("%f", &b);
	vol = ((1.0/3.0)*((h*d)+d))/b;
	printf("Volume of the tromboloid is: %f ",vol);
	return 0;
}

