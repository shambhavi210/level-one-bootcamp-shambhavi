//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>

float input(char s[10]) {
    printf("Enter the value of %s:",s);
    float n;
    scanf("%f", &n);
    return n;
}

float compute_distance(float x1, float y1, float x2, float y2) {
    float distance = sqrt(pow((x2-x1), 2) + pow((y2-y1), 2));
    return distance;
}

void output(float distance) {
    printf("The distance between the 2 given points is %f", distance);
}

int main() {
    
    float x1, x2, y1, y2, distance;
    x1 = input("x1");
    y1 = input("y1");
    x2 = input("x2");
    y2 = input("y2");
    distance = compute_distance(x1, y1, x2, y2);
    output(distance);
    return 0;
}