//Write a program to find the sum of n different numbers using 4 functions
#include<stdio.h>
int input()
{
    int x; 
    scanf("%d", &x);
    return x;
}

int add(int a, int b)
{
    return a+b;
}

void display(int sum)
{
    printf("The sum is %d\n",sum);
}

int main() {
	int n, elt, sum=0;
	printf("Enter the number of elements: ");
	scanf("%d", &n);
	printf("Enter the elements : \n");
	for (int i=0; i<n; i++) {
		elt = input();
	    sum = add(sum, elt);
	}
	display(sum);
	return 0;
}