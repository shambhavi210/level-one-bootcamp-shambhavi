//Write a program to add two user input numbers using 4 functions.
#include<stdio.h>
 
int input(){
	int n;
	printf("Enter the number to be added: ");
	scanf("%d",&n);
	return n;
}

int sum(int a, int b){
	int sum;
	sum = a+b;
	return sum;
}

void display(int a, int b, int c){
	printf("Sum of the two numbers %d and %d is %d\n",a,b,c);
}

int main(){
	int n1,n2,add;
	n1 = input();
	n2 = input();
	add = sum(n1,n2);
	display(n1,n2,add);
	return 0;
}
