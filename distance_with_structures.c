//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>

struct coordinates
{
    float x, y;
};
 
float compute_distance(struct coordinates a, struct coordinates b)
{
    float result;
    result = sqrt(pow((a.x - b.x),2) + pow((a.y - b.y),2));
    return result;
}
 
int main()
{
    struct coordinates a,b;
    printf("\nEnter (x,y) Coordinate of First Point :\n");
    scanf("%f", &a.x);
    scanf("%f", &a.y);  
    printf("\nEnter (x,y) Coordinate of Second Point :\n");
    scanf("%f", &b.x);
    scanf("%f", &b.y);
    printf("\nDistance between two points is: %f\n", compute_distance(a, b));
    return 0;
}