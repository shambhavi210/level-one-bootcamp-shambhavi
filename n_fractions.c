//WAP to find the sum of n fractions.
#include<stdio.h>
typedef struct
{
    int n;
    int d;
}frac;
int num;
frac input()
{
    frac data;
    printf("Enter the fraction:\n");
    scanf("%d",&data.n);
    scanf("%d",&data.d);
    return data;
}
int gcd(int a, int b)
{
    if(a==0)
    return b;
    else
    return gcd(b%a,a);
}
int lcm(frac arr[])
{
    int fd=arr[0].d;
    for(int i=1;i<num;i++)
    {
        fd=(fd*arr[i].d)/gcd(fd,arr[i].d);
    }
    return fd;
}
int add(frac arr1[], int fd)
{
    int sum=0;
    for(int i=0;i<num;i++)
    {
        sum=sum+(fd/arr1[i].d)*arr1[i].n;
    }
    return sum;
}
frac simplify(frac sum)
{
    int hcf=gcd(sum.n,sum.d);
    sum.n/=hcf;
    sum.d/=hcf;
    return sum;
}
void display(frac sum)
{
    if(sum.n==0)
    printf("Sum=0\n");
    else if(sum.n==sum.d)
    printf("Sum=1\n");
    else
    printf("Sum=%d/%d\n",sum.n,sum.d);
}
int main()
{
    frac sum;
    printf("Enter the number of fractions:\n");
    scanf("%d",&num);
    frac arr[num];
    for(int i=0;i<num;i++)
    {
        arr[i]=input();
    }
    sum.d=lcm(arr);
    sum.n=add(arr,sum.d);
    sum=simplify(sum);
    display(sum);
}