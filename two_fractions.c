//WAP to find the sum of two fractions.
#include <stdio.h>
#include<stdlib.h>

typedef struct
{
	int n;
	int d;
}fraction;

fraction sum(fraction f1, fraction f2){
    fraction final;
    fraction result={(f1.n * f2.d) + (f2.n * f1.d), f1.d * f2.d};
    for(int i=1;i<=result.n || i<=result.d;i++)
    {
        if(result.n%i==0 && result.d%i==0){
            final.n=result.n/i;
            final.d=result.d/i;
        }
    }
     return final;
 }

fraction input() {
    fraction data;
    printf("Enter the numerator :\n");
    scanf("%d",&data.n);
    printf("Enter the denominator :\n");
    scanf("%d",&data.d);
    return data;
}

void output(fraction f1, fraction f2, fraction result){
    if(result.n%result.d == 0)
        { printf("\n%d/%d + %d/%d = %d\n", f1.n, f1.d, f2.n, f2.d, result.n/result.d); }
    else
        { printf("%d/%d + %d/%d = %d/%d\n", f1.n, f1.d, f2.n, f2.d, result.n,result.d); }
}

int main(){
    fraction f1, f2;
    f1= input();
    f2=input();
    fraction result = sum(f1, f2);
    output(f1,f2,result);
    return 0;
    
}